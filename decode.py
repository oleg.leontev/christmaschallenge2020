import datetime as dt
import math
import os
import random
import sys

from collections import Counter, defaultdict, namedtuple
from functools import partial
from operator import itemgetter

import numpy as np
from PIL import Image, ImageOps
from scipy.cluster.hierarchy import fclusterdata
import zbar

MIN_SQUARES_TO_DETECT = 16
MIN_SQUARE_SIDE = 24
GRID_POSITION_THRESHOLD = 2
BLACK_COLOR_THRESHOLD = 48
WHITE_COLOR_THRESHOLD = 256 - 48
QR_CODE_GRAY_THRESHOLD = 100
SQUARE_EXPANSION = 1
SQUARE_MARGIN = 0
MARGIN_FILLER_GRAYSCALE_COLOR = 255
BLANK_AREA_RATIO = 0.2
USE_ROTATIONS = False
COLOR_BLACK = 0x000000
COLOR_WHITE = 0xFFFFFF

AREA_CENTER = 0
AREA_N = 1
AREA_S = 2
AREA_W = 3
AREA_E = 4
AREA_NW = 5
AREA_NE = 6
AREA_SE = 7
AREA_SW = 8

qr_codes_analyzed = 0
found_text = None


def main():
    file_path = get_path_from_args()
    data = get_image_as_data(file_path)
    square_size, square_positions = detect_squares(data)
    square_positions = align_squares(square_positions)

    grid_size = round(math.sqrt(len(square_positions)))
    qr_code_size = square_size * grid_size

    pieces = get_pieces(data, square_positions, square_size)
    buckets, pieces_with_rotations = distribute_pieces(pieces)
    # print("Buckets debug output:")
    # for k, v in buckets.items():
    #    print(f"{k} -- {sorted(list(set(b.original_id for b in v)))}")

    brute_force_with_buckets(qr_code_size, grid_size,
                             buckets, pieces_with_rotations)
    if found_text is None:
        print("No luck >_<")
    else:
        print(f"href={found_text}")


def get_path_from_args() -> str:
    assert len(sys.argv) > 1, \
        "File name or path should be supplied"
    file_path = sys.argv[1]
    assert os.path.isfile(file_path), \
        f"File >{file_path}< should be available"
    return file_path


def get_image_as_data(file_path: str) -> np.ndarray:
    img = Image.open(file_path)
    return np.array(img, dtype="uint32")


def get_pieces(data: np.ndarray,
               square_positions: list[tuple[int, int]],
               square_size: int) -> list[np.ndarray]:
    return [data[
            y0:y0 + square_size,
            x0:x0 + square_size]
            for y0, x0 in square_positions]


def glue_pieces(image_size: int,
                grid_size: int,
                pieces: list[np.ndarray],
                order: list[int]) -> np.ndarray:
    assert len(order) == grid_size ** 2, \
        "Pieces order should be for a complete square"
    assert max(order) < len(pieces), \
        "Order indexes should be valid"
    assert len(order) == len(set(order)), \
        "Order indexes should be unique"

    _, _, num_channels = pieces[0].shape
    image_size += grid_size * SQUARE_MARGIN
    data = np.full((image_size, image_size, num_channels),
                   fill_value=MARGIN_FILLER_GRAYSCALE_COLOR,
                   dtype="uint8")
    order_iter = iter(order)
    step = image_size // grid_size
    for y0 in range(0, image_size, step):
        for x0 in range(0, image_size, step):
            piece = pieces[next(order_iter)]
            h, w, _ = piece.shape
            data[y0:y0 + h, x0:x0 + w] = piece

    # Use Image.show() only with breakpoints!!!
    # Image.fromarray(np.array(data, dtype="uint8")).show()
    return data


def try_parse_qr_code(data: np.ndarray):
    global qr_codes_analyzed
    global found_text
    try:
        qr_codes_analyzed += 1
        img = Image.fromarray(data)
        img = ImageOps.grayscale(img)

        data = np.array(img)
        data[data < QR_CODE_GRAY_THRESHOLD] = 0
        data[data > 0] = 255

        scr = zbar.Scanner()
        text = None
        for _, result, _, _ in scr.scan(data):
            text = result.decode()
    except:
        text = ""
        print("This image broke the scanner")

    if text is not None and len(text) > 0:
        found_text = text


PieceRotation = namedtuple("PieceRotation", "original_id rotation_id")


def distribute_pieces(pieces: list[np.ndarray]) \
        -> tuple[dict[int, set[PieceRotation]], list[np.ndarray]]:
    buckets = {
        AREA_CENTER: set(),
        AREA_N: set(),
        AREA_S: set(),
        AREA_W: set(),
        AREA_E: set(),
        AREA_NW: set(),
        AREA_NE: set(),
        AREA_SE: set(),
        AREA_SW: set()
    }

    pieces_with_rotations = list(pieces)
    for piece_id, piece in enumerate(pieces):
        square_size, _, _ = piece.shape
        blank_area_width = round(square_size * BLANK_AREA_RATIO)
        bucket_ids = determine_bucket_ids(piece, blank_area_width)
        for bid in bucket_ids:
            buckets[bid].add(PieceRotation(piece_id, piece_id))

        # Add rotations
        if USE_ROTATIONS:
            rotation = np.rot90(piece)
            for _ in range(3):
                pieces_with_rotations.append(rotation)
                rotation_id = len(pieces_with_rotations) - 1
                bucket_ids = determine_bucket_ids(rotation, blank_area_width)
                for bid in bucket_ids:
                    buckets[bid].add(PieceRotation(piece_id, rotation_id))
                rotation = np.rot90(rotation)

    return buckets, pieces_with_rotations


def determine_bucket_ids(piece: np.ndarray,
                         blank_area_width: int) -> set[int]:
    area_ids = set()

    # Exclude 1 border pixel, just for fear
    # that it may contain a piece of grid
    if np.average(piece[1:blank_area_width]) >= WHITE_COLOR_THRESHOLD:
        area_ids.add(AREA_N)

    if np.average(piece[-blank_area_width:-1]) >= WHITE_COLOR_THRESHOLD:
        area_ids.add(AREA_S)

    if np.average(piece[:, 1:blank_area_width]) >= WHITE_COLOR_THRESHOLD:
        area_ids.add(AREA_W)

    if np.average(piece[:, -blank_area_width:-1]) >= WHITE_COLOR_THRESHOLD:
        area_ids.add(AREA_E)

    bucket_ids = set()

    if AREA_N in area_ids and AREA_W in area_ids:
        bucket_ids.add(AREA_NW)

    if AREA_N in area_ids and AREA_E in area_ids:
        bucket_ids.add(AREA_NE)

    if AREA_S in area_ids and AREA_E in area_ids:
        bucket_ids.add(AREA_SE)

    if AREA_S in area_ids and AREA_W in area_ids:
        bucket_ids.add(AREA_SW)

    if len(bucket_ids) == 0:
        bucket_ids = area_ids

    if len(bucket_ids) == 0:
        bucket_ids = {AREA_CENTER}

    return bucket_ids


def brute_force_with_buckets(image_size: int,
                             grid_size: int,
                             buckets: dict[int, set[PieceRotation]],
                             pieces_with_rotations: list[np.ndarray]):
    grid_areas = prepare_grid_areas(grid_size)
    available_pieces = frozenset(range(grid_size ** 2))
    order = []
    loop_for_cell(order=order,
                  available_pieces=available_pieces,
                  grid_size=grid_size,
                  grid_areas=grid_areas,
                  image_size=image_size,
                  buckets=buckets,
                  pieces_with_rotations=pieces_with_rotations)


def loop_for_cell(order: list[int],
                  available_pieces: frozenset,
                  grid_size: int,
                  grid_areas: np.ndarray,
                  image_size: int,
                  buckets: dict[int, set[PieceRotation]],
                  pieces_with_rotations: list[np.ndarray]):
    cell_num = len(order)

    # Show progress at some level of recursion
    if cell_num == (grid_size ** 2) // 2 + 1:
        print(f"[{dt.datetime.now()}] Still working: {qr_codes_analyzed} "
              "combinations analyzed")

    # Exit recursion
    if cell_num == grid_size ** 2:
        image_data = glue_pieces(image_size=image_size,
                                 grid_size=grid_size,
                                 pieces=pieces_with_rotations,
                                 order=order)
        try_parse_qr_code(image_data)
        return

    # Iterate all available pieces for the next cell
    grid_x = cell_num % grid_size
    grid_y = cell_num // grid_size
    area_id = grid_areas[grid_y, grid_x]

    options_for_cell = list(p for p in buckets[area_id]
                            if p.original_id in available_pieces)
    if len(options_for_cell) == 0:
        return

    # Try Monte Carlo :))
    random.shuffle(options_for_cell)

    for piece_id, rotation_id in options_for_cell:
        new_order = order + [rotation_id]
        remaining_pieces = frozenset(available_pieces - {piece_id})
        loop_for_cell(order=new_order,
                      available_pieces=remaining_pieces,
                      grid_size=grid_size,
                      grid_areas=grid_areas,
                      image_size=image_size,
                      buckets=buckets,
                      pieces_with_rotations=pieces_with_rotations)


def prepare_grid_areas(grid_size: int) -> np.ndarray:
    """
    Prepares a 2D array with area ID put to each square.
    """
    grid_areas = np.full((grid_size, grid_size), AREA_CENTER)
    grid_areas[0] = AREA_N
    grid_areas[-1] = AREA_S
    grid_areas[:, 0] = AREA_W
    grid_areas[:, -1] = AREA_E
    grid_areas[0, 0] = AREA_NW
    grid_areas[0, -1] = AREA_NE
    grid_areas[-1, -1] = AREA_SE
    grid_areas[-1, 0] = AREA_SW
    return grid_areas


def detect_squares(data: np.ndarray) -> tuple[int, list[tuple[int, int]]]:
    data = reduce_to_color_numbers(simplify_palette(data))

    low = MIN_SQUARE_SIDE
    high = min(data.shape) // int(math.sqrt(MIN_SQUARES_TO_DETECT))
    prev_squares = squares = find_all_bw_squares_of_size(low, data)
    checked_values = {low}
    last_size = low
    assert len(squares) >= MIN_SQUARES_TO_DETECT, \
        "With minimal square size enough squares should be detected"

    while high > low:
        if high not in checked_values:
            squares = find_all_bw_squares_of_size(high, data)
            checked_values.add(high)
            if len(squares) >= MIN_SQUARES_TO_DETECT:
                prev_squares = squares
                last_size = high
                break

        middle = (high + low) // 2
        if middle in checked_values:
            break

        squares = find_all_bw_squares_of_size(middle, data)
        checked_values.add(middle)
        if len(squares) >= MIN_SQUARES_TO_DETECT:
            prev_squares = squares
            last_size = middle
            low = middle
        else:
            high = middle

    return (last_size + SQUARE_EXPANSION * 2), \
           [(y - SQUARE_EXPANSION, x - SQUARE_EXPANSION)
            for y, x in prev_squares]


def find_all_bw_squares_of_size(side_size: int,
                                data: np.ndarray) -> list[tuple[int, int]]:
    squares = []
    height, width = data.shape
    for y0 in range(0, height - side_size):
        for x0 in range(0, width - side_size):
            square = data[y0:y0 + side_size, x0:x0 + side_size]
            if np.all(np.logical_or(square == COLOR_BLACK,
                                    square == COLOR_WHITE)):
                squares.append((y0, x0))

    if len(squares) > 2:
        squares = average_for_clusters(squares,
                                       distance_threshold=side_size / 3)
    print(f"Side {side_size}: {len(squares)} possible squares")
    return squares


def average_for_clusters(squares: list[tuple[int, int]],
                         distance_threshold: float) -> list[tuple[int, int]]:
    averages = []
    squares_data = np.array([np.array(s) for s in squares])
    cluster_map = fclusterdata(squares_data, distance_threshold,
                               criterion="distance")
    for cluster_no in set(cluster_map):
        selected_squares = squares_data[cluster_map == cluster_no]
        y = np.average(selected_squares[:, 0])
        x = np.average(selected_squares[:, 1])
        averages.append((round(y), round(x)))

    return averages


def align_squares(squares: list[tuple[int, int]]) -> list[tuple[int, int]]:
    """
    Exclude stray B&W squares that cannot form a grid.
    """
    new_list = list(squares)
    for key_index in [
        0,  # arrange in rows
        1  # arrange in columns
    ]:
        arranged = defaultdict(list)
        key_selector = itemgetter(key_index)

        # Group squares with close Y or X positions
        for square in new_list:
            position = key_selector(square)
            for key in arranged:
                if abs(key - position) <= GRID_POSITION_THRESHOLD:
                    arranged[key].append(square)
                    break
            else:  # for key
                arranged[position].append(square)

        # Filter out any stray squares and do the alignment
        new_list = []
        for group in arranged.values():
            if len(group) < 2:
                continue  # Exclude squares that do not align with others
            position_counter = Counter(key_selector(square)
                                       for square in group)
            most_common_position = \
                position_counter.most_common(1)[0][0]
            for square in group:
                if key_index == 0:
                    new_list.append((most_common_position, square[1]))
                else:
                    new_list.append((square[0], most_common_position))

    # Assert that the grid is a full square
    num_squares = len(new_list)
    grid_size = round(math.sqrt(num_squares))
    assert num_squares == grid_size ** 2, \
        f"Finally found {num_squares} fragments, should be a full square"

    return new_list


def reduce_to_color_numbers(data: np.ndarray) -> np.ndarray:
    return data[:, :, 0] * 0x10000 + \
           data[:, :, 1] * 0x100 + \
           data[:, :, 2]


def simplify_palette(data: np.ndarray, factor: int = 2) -> np.ndarray:
    height, width, channels_num = data.shape
    pixel_sequence = data.reshape((height * width, channels_num))
    posterized = map(partial(get_simplified_color, factor), pixel_sequence)
    posterized = np.array(list(posterized)).reshape(data.shape)
    # Image.fromarray(np.array(posterized, dtype="uint8")).show()
    return posterized


def get_simplified_color(factor: int,
                         channels: np.ndarray) -> np.ndarray:
    if np.sum(channels) <= BLACK_COLOR_THRESHOLD * len(channels):
        new_color = np.array(channels, dtype="uint32")
        new_color[:] = 0
        return new_color

    new_color = channels * 256.0 / (max(channels) + 1)
    step = 256 // factor
    new_color = np.round(new_color / step) * step - 1
    return np.array(new_color, dtype="uint32")


if __name__ == '__main__':
    main()
